/*!
 * jQuery plugin for shikari.do
 * ver. 1.0.4
 * Licensed under the MIT license
 */

/*global jQuery */
;(function ($, window, document, undefined) {
    "use strict";

    var pluginName = 'shikari',
        fileName = 'jquery.shikari',
        pluginPath = null,
        defaultCssUrl = null,
        baseUrl = 'https://www.shikari.do/',
        defaults = {
            category: [1, 2],
            limit: 10,
            page: 1,
            url: 'https://shikari.do/api/sr-demo',
            autoSize: true,
            beforeRender: function(){
                var clip = $('#shikari-do-svg-clip');
                if (!clip.length) {
                    $("body").append('<svg id="shikari-do-svg-clip" style="width: 0;height: 0;"><defs><clipPath id="shikari-do-svg-clip-hexagon" clipPathUnits="objectBoundingBox"><polygon points="0.50 0, 0.95 0.25, 0.95 0.75, 0.5 1, 0.05 0.75, 0.05 0.25" /></clipPath></defs></svg>');
                }
            },
            partnerId: null,
            referralUrl: null,
            loadStyles: null,
            templateUrl: null,
            template: '<div class="shikari-do-request"><div class="shikari-do-request-ava"><svg class="shikari-do-request-ava-svg"><image class="shikari-do-request-ava-image" style="clip-path: url(\'#shikari-do-svg-clip-hexagon\')" xmlns:xlink="https://www.w3.org/1999/xlink" xlink:href="{{user_ava}}"></image> </svg> </div> <div class="shikari-do-request-info"> <div class="shikari-do-request-info-snippet">{{snippet}} </div> <div class="shikari-do-request-info-meta"> <span class="shikari-do-request-info-meta-date">{{datetime}}</span> <img src="{{resourse_fav}}" class="shikari-do-request-info-meta-favicon"><span class="shikari-do-request-info-meta-resourse">{{resourse_name}}</span></div></div></div>'
        };

    // The actual plugin constructor
    function Plugin(element, options) {
        var $plugin = this, widget, content, footer;

        this.element = element;

        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        if ($.isNumeric(this.settings.category)) {
            this.settings.category = [this.settings.category];
        }
        widget = $('<div class="shikari-do-widget" style="position: relative; display: none;"></div>');
        content = $('<div class="shikari-do-content" style="position: relative; clear: both;"></div>').appendTo(widget);
        footer = $('<div class="shikari-do-footer"></div>')
            .append('<a href="' + this.getReferralUrl(this.settings.partnerId) + '" target="_blank">shikari.do</a>')
            .appendTo(widget);
        $(this.element).append(widget);

        if (this.settings.autoSize) {
            this._resizeWidget(widget, content, footer, 'bottom');
        }

        $.data(this.element, 'shikari-do-widget', widget);
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
        init: function () {
            var $plugin = this;
            if ($plugin.settings.loadStyles === true) {
                $plugin.settings.loadStyles = defaultCssUrl;
            }
            $plugin.load();
        },
        load: function () {
            var $plugin = this,
                url = $plugin.settings.url;

            $.each($plugin.settings.category, function (index, category) {
                url += (index ? "&" : "?") + "category[]=" + category;
            });
            url += "&limit=" + $plugin.settings.limit + "&page=" + $plugin.settings.page;
            $.get({
                url: url,
                dataType: "json",
                context: $plugin.element
            }).then(function (response) {
                if (response && response.status === "ok") {
                    if (response.result) {
                        if ($plugin.settings.loadStyles && $.type($plugin.settings.loadStyles) === "string") {
                            $plugin.loadStyle($plugin.settings.loadStyles);
                        }
                        if ($plugin.settings.templateUrl) {
                            $plugin.loadTemplate($plugin.settings.templateUrl).then(function(){
                                $plugin.render(response.result);
                            });
                        } else {
                            $plugin.render(response.result);
                        }
                    }
                }
            });
        },
        getReferralUrl: function(partnerId, categoryId) {
            var $plugin = this, url;

            if ($plugin.settings.referralUrl) {
                url = $plugin.settings.referralUrl;
            } else if (categoryId) {
                url = baseUrl + "category/" + categoryId + "?utm_medium=referral&utm_content=" + partnerId + '&utm_campaign=widget';
            } else {
                url = baseUrl + "?utm_medium=referral&utm_content=" + partnerId + '&utm_campaign=widget';
            }
            return url;
        },
        loadStyle: function (url) {
            var $plugin = this;
            if (document.createStyleSheet){
                document.createStyleSheet(url);
            }
            else {
                $("head").append($('<link rel="stylesheet" href="' + url + '" type="text/css">'));
            }
        },
        loadTemplate: function(url) {
            var $plugin = this;
            return $.get({
                url: url,
                dataType: "html"
            }).then(function (response) {
                $plugin.settings.template = response;
            });
        },
        /**
         * Render array of items
         *
         * @param data
         */
        render: function(data) {
            var $plugin = this,
                readyToRender = true,
                widget = $.data($plugin.element, 'shikari-do-widget');

            if ($plugin.settings.beforeRender && $.type($plugin.settings.beforeRender) === "function") {
                readyToRender = $plugin.settings.beforeRender(data, $plugin.settings.template);
            } else {
                readyToRender = true;
            }
            if (readyToRender !== false) {
                $.each(data, function (index, item) {
                    $plugin.renderItem(item);
                });
                widget.css("display", "block");
            }
        },
        /**
         * Render single item
         *
         * @param item
         */
        renderItem: function (item) {
            var $plugin = this,
                itemElement = $($plugin.parseTemplate($plugin.settings.template, item)),
                widget = $.data($plugin.element, 'shikari-do-widget'),
                content = widget.find('.shikari-do-content');

            // fix FF (width & height of avatars)
            itemElement.find('.shikari-do-request-ava-image').each(function(){
                var image = $(this), w = image.css('width'), h = image.css('height' );
                if (!image.attr('width')) {
                    image.attr('width', w);
                }
                if (!image.attr('height')) {
                    image.attr('height', h);
                }
            });

            itemElement.children().click(function(){
                window.open($plugin.getReferralUrl($plugin.settings.partnerId, item.category_id), '_blank');
            });
            content.append(itemElement);
            if ($plugin.settings.autoSize) {
                $plugin._resizeItem(content, itemElement);
            }
        },
        parseTemplate: function (template, data) {
            var $plugin = this;
            return template.replace(/\{\{([\w\.]*)\}\}/g, function (str, key) {
                var keys = key.split("."), v = data[keys.shift()];
                for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]];
                return (typeof v !== "undefined" && v !== null) ? v : "";
            });
        },
        _calcWidth: function(el) {
            var $plugin = this,
                width = 0;
            el = $(el);

            if (el.length >= 1) {
                el.first();
            } else {
                return 0;
            }
            width = el.width();
            if (!width) {
                $(el).children().each(function(){
                    var childWidth = $plugin._calcWidth(this);
                    if (childWidth && childWidth > width) {
                        width = childWidth;
                    }
                });
            }
            return width;
        },
        _resizeWidget: function(widget, content, brandblock, position) {
            var $plugin = this,
                maxHeight = (widget.parent().innerHeight() ? widget.parent().innerHeight() : widget.parent().height()),
                maxWidth = (widget.parent().innerWidth() ? widget.parent().innerWidth() : widget.parent().width());

            if ($.isNumeric(maxHeight) && maxHeight > 0) {
                if (widget.css('border-top-width')) {
                    maxHeight -= parseInt(widget.css('border-top-width'));
                }
                if (widget.css('border-bottom-width')) {
                    maxHeight -= parseInt(widget.css('border-bottom-width'));
                }
            }
            if ($.isNumeric(maxWidth) && maxWidth > 0) {
                if (widget.css('border-left-width')) {
                    maxWidth -= parseInt(widget.css('border-left-width'));
                }
                if (widget.css('border-right-width')) {
                    maxWidth -= parseInt(widget.css('border-right-width'));
                }
            }
            if (position === 'top' || position === 'bottom') {
                maxHeight = maxHeight - (brandblock.outerHeight() ? brandblock.outerHeight() : brandblock.height());
            }
            if (position === 'left' || position === 'right') {
                maxWidth = maxWidth - (brandblock.outerWidth() ? brandblock.outerWidth() : brandblock.width());
            }
            if (maxHeight) {
                content.css({maxHeight: maxHeight, overflowY: 'hidden'});
            }
            if (maxWidth) {
                content.css({maxWidth: maxWidth});
            }
        },
        _resizeItem: function (content, item) {
            var $plugin = this,
                ava = item.find('.shikari-do-request-ava'),
                avaWidth = ava.length ? $plugin._calcWidth(ava) : 0,
                contentWidth = (content.css('max-width') ? parseInt(content.css('max-width')) : content.innerWidth()),
                itemMargin = parseInt(item.css('margin-left')) + parseInt(item.css('margin-right')),
                maxItemWidth = contentWidth ? (contentWidth - itemMargin) : 0,
                maxItemInfo = 0;

            if (maxItemWidth) {
                item.css('max-width', maxItemWidth);
                if (avaWidth) {
                    maxItemInfo = maxItemWidth - avaWidth;
                }
                if (maxItemInfo) {
                    item.find('.shikari-do-request-info').css('max-width', maxItemInfo);
                }
            }
        }
    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (partnerId, options) {
        if ($.isPlainObject(partnerId)) {
            options = partnerId;
        } else if ($.isNumeric(partnerId)){
            options = options || {};
            options.partnerId = partnerId;
        }
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" +
                    pluginName, new Plugin(this, options));
            }
        });
    };

    $('script').each(function(){
        var src = $(this).prop('src'),
            pos = src.lastIndexOf('/'),
            path = src.substr(0, pos + 1),
            name = src.substr(pos + 1);

        if (name.indexOf(fileName) === 0 && name.substr(-3) === '.js') {
            pluginPath = path;
            defaultCssUrl = path + fileName + (name.substr(-7) === '.min.js' ? '.min.css' : '.css');
        }
    });
})(jQuery, window, document);

// EOF